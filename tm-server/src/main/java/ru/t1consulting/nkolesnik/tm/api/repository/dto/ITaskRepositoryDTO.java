package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    List<TaskDTO> findAll(@Nullable Sort sort);

    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    List<TaskDTO> findAll(@Nullable Comparator comparator);

    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeByProjectId(@Nullable String projectId);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);

}
