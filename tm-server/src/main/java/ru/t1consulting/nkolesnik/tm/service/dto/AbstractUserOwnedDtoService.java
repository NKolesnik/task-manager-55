package ru.t1consulting.nkolesnik.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    public AbstractUserOwnedDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
