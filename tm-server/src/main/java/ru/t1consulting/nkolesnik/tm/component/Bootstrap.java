package ru.t1consulting.nkolesnik.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.*;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.endpoint.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.listener.EntityListener;
import ru.t1consulting.nkolesnik.tm.listener.OperationEventListener;
import ru.t1consulting.nkolesnik.tm.service.*;
import ru.t1consulting.nkolesnik.tm.service.dto.ProjectDtoService;
import ru.t1consulting.nkolesnik.tm.service.dto.ProjectTaskDtoService;
import ru.t1consulting.nkolesnik.tm.service.dto.TaskDtoService;
import ru.t1consulting.nkolesnik.tm.service.dto.UserDtoService;
import ru.t1consulting.nkolesnik.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);

    @Getter
    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskDtoService(projectService, taskService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(calculatorEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(taskEndpoint);
        registry(projectTaskEndpoint);
        registry(projectEndpoint);
        registry(authEndpoint);
    }

    @SneakyThrows
    public void initJMS() {
        BasicConfigurator.configure();
        EntityListener.setConsumer(new OperationEventListener());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initJMS();
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER SERVER**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
    }

    private void initDemoData() {
        @Nullable final UserDTO test = userService.findByLogin("test");
        if (test == null) {
            userService.create("test", "test", "test@test.email.ru", Role.USUAL);
        }
        @Nullable final UserDTO admin = userService.findByLogin("admin");
        if (admin == null) {
            userService.create("admin", "admin", "admin@admin.email.ru", Role.ADMIN);
        }
        if (admin != null) {
            @NotNull final List<ProjectDTO> projects = projectService.findAll(admin.getId());
            if (projects.isEmpty()) {
                projectService.create(admin.getId(), "admin project", "admin project description");
            }
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

}