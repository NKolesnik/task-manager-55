package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IConnectionService getConnectionService();

}
