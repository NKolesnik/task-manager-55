package ru.t1consulting.nkolesnik.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskDtoServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    @Nullable
    private static final String NULL_TASK_NAME = null;

    @Nullable
    private static final String NULL_TASK_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_TASK_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final TaskDTO NULL_TASK = null;

    private static final long REPOSITORY_SIZE = 10L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;
    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();
    @NotNull
    private static final String PROJECT_ID = UUID.randomUUID().toString();
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);
    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);
    @NotNull
    private final String taskId = UUID.randomUUID().toString();
    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);
    @NotNull
    private List<TaskDTO> tasks;

    @NotNull
    private TaskDTO task;

    private void createOneProject() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(PROJECT_ID);
        project.setUserId(USER_ID);
        project.setName("TEST");
        project.setDescription("TEST");
        project.setStatus(Status.IN_PROGRESS);
        projectService.add(project);
    }

    @Before
    public void setup() {
        createUser();
        createOneProject();
        task = createOneTask();
        tasks = createManyTasks();
    }

    @After
    public void cleanup() {
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void add() {
        taskService.add(task);
        Assert.assertEquals(1, taskService.getSize());
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE + 1, taskService.getSize());
    }

    @Test
    public void set() {
        taskService.set(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void existById() {
        taskService.add(task);
        Assert.assertFalse(taskService.existsById(NULL_TASK_ID));
        Assert.assertFalse(taskService.existsById(EMPTY_TASK_ID));
        Assert.assertTrue(taskService.existsById(taskId));
    }

    @Test
    public void findAll() {
        taskService.add(tasks);
        Assert.assertEquals(tasks.size(), taskService.findAll().size());
    }

    @Test
    public void findById() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(EMPTY_TASK_ID));
        Assert.assertNull(taskService.findById(UUID.randomUUID().toString()));
        @Nullable final TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void remove() {
        taskService.add(task);
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.remove(NULL_TASK));
        taskService.remove(task);
        @Nullable final TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void removeById() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(EMPTY_TASK_ID));
        taskService.removeById(taskId);
        @Nullable final TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void clear() {
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
        taskService.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void getSize() {
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void addWithUserId() {
        taskService.add(USER_ID, task);
        Assert.assertEquals(1, taskService.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(EMPTY_USER_ID, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.add(USER_ID, NULL_TASK));
    }

    @Test
    public void existByIdWithUserId() {
        taskService.add(USER_ID, task);
        Assert.assertTrue(taskService.existsById(USER_ID, taskId));
        Assert.assertFalse(taskService.existsById(NULL_USER_ID, taskId));
        Assert.assertFalse(taskService.existsById(EMPTY_USER_ID, taskId));
        Assert.assertFalse(taskService.existsById(USER_ID, NULL_TASK_ID));
        Assert.assertFalse(taskService.existsById(USER_ID, EMPTY_TASK_ID));
        Assert.assertTrue(taskService.existsById(USER_ID, taskId));
    }

    @Test
    public void findAllWithUserId() {
        for (TaskDTO task : tasks)
            taskService.add(USER_ID, task);
        taskService.add(USER_ID, task);
        Assert.assertEquals(REPOSITORY_SIZE + 1, taskService.findAll(USER_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void findByIdWithUserId() {
        for (TaskDTO task : tasks)
            taskService.add(USER_ID, task);
        taskService.add(USER_ID, task);
        @Nullable final TaskDTO repositoryTask = taskService.findById(USER_ID, taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(USER_ID, NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(USER_ID, EMPTY_TASK_ID));
    }

    @Test
    public void removeWithUserId() {
        taskService.add(USER_ID, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(EMPTY_USER_ID, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.remove(USER_ID, NULL_TASK));
        taskService.remove(USER_ID, task);
        @Nullable final TaskDTO repositoryTask = taskService.findById(USER_ID, taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void removeByIdWithUserId() {
        taskService.add(USER_ID, task);
        for (TaskDTO task : tasks)
            taskService.add(USER_ID, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(USER_ID, NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(USER_ID, EMPTY_TASK_ID));
        taskService.removeById(USER_ID, task.getId());
        @Nullable final TaskDTO repositoryTask = taskService.findById(USER_ID, taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void clearWithUserId() {
        taskService.add(USER_ID, task);
        Assert.assertEquals(1, taskService.getSize(USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(EMPTY_USER_ID));
        taskService.clear(USER_ID);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        taskService.add(USER_ID, task);
        Assert.assertEquals(1, taskService.getSize(USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(EMPTY_USER_ID));
        taskService.clear(USER_ID);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void create() {
        taskService.add(USER_ID, task);
        Assert.assertEquals(1, taskService.getSize(USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(NULL_USER_ID, TASK_NAME_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID, NULL_TASK_NAME));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.create(USER_ID, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION)
        );
    }

    @Test
    public void updateById() {
        taskService.add(USER_ID, task);
        Assert.assertEquals(1, taskService.getSize(USER_ID));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> taskService.updateById(NULL_USER_ID, taskId, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> taskService.updateById(USER_ID, NULL_TASK_ID, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateById(USER_ID, taskId, NULL_TASK_NAME, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateById(USER_ID, taskId, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION)
        );
    }

    @Test
    public void changeStatusById() {
        taskService.add(USER_ID, task);
        Assert.assertEquals(1, taskService.getSize(USER_ID));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> taskService.changeTaskStatusById(NULL_USER_ID, taskId, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> taskService.changeTaskStatusById(USER_ID, NULL_TASK_ID, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> taskService.changeTaskStatusById(USER_ID, taskId, NULL_STATUS)
        );
    }

    @Test
    public void findAllByProjectId() {
        for (TaskDTO task : tasks)
            taskService.add(USER_ID, task);
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.findAllByProjectId(NULL_USER_ID, taskId));
        Assert.assertEquals(REPOSITORY_SIZE, taskService.findAllByProjectId(USER_ID, PROJECT_ID).size());
    }

    @NotNull
    private TaskDTO createOneTask() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(taskId);
        task.setUserId(USER_ID);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setProjectId(PROJECT_ID);
        return task;
    }

    @NotNull
    private List<TaskDTO> createManyTasks() {
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            TaskDTO task = new TaskDTO();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(USER_ID);
            task.setProjectId(PROJECT_ID);
            tasks.add(task);
        }
        return tasks;
    }

    private void createUser() {
        @NotNull final UserDTO user = new UserDTO();
        user.setId(USER_ID);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        userService.add(user);
    }

}
