package ru.t1consulting.nkolesnik.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class ProjectDtoServiceTest {

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    @Nullable
    private static final String NULL_PROJECT_NAME = null;

    @Nullable
    private static final String NULL_PROJECT_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_PROJECT_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final ProjectDTO NULL_PROJECT = null;

    private static final long REPOSITORY_SIZE = 10L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private List<ProjectDTO> projects;

    @NotNull
    private ProjectDTO project;

    @Before
    public void setup() {
        createUser();
        project = createOneProject();
        projects = createManyProjects();
    }

    @After
    public void cleanup() {
        projectService.clear();
        userService.clear();
    }

    @Test
    public void add() {
        projectService.add(project);
        Assert.assertEquals(1, projectService.getSize());
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE + 1, projectService.getSize());
    }

    @Test
    public void set() {
        projectService.set(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void existById() {
        projectService.add(project);
        Assert.assertFalse(projectService.existsById(NULL_PROJECT_ID));
        Assert.assertFalse(projectService.existsById(EMPTY_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(project.getId()));
    }

    @Test
    public void findAll() {
        projectService.add(projects);
        Assert.assertEquals(projects.size(), projectService.findAll().size());
    }

    @Test
    public void findById() {
        projectService.add(project);
        projectService.add(projects);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(EMPTY_PROJECT_ID));
        Assert.assertNull(projectService.findById(UUID.randomUUID().toString()));
        @Nullable final ProjectDTO repositoryProject = projectService.findById(project.getId());
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void remove() {
        projectService.add(project);
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.remove(NULL_PROJECT));
        projectService.remove(project);
        @Nullable final ProjectDTO repositoryProject = projectService.findById(project.getId());
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void removeById() {
        projectService.add(project);
        projectService.add(projects);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(EMPTY_PROJECT_ID));
        projectService.removeById(project.getId());
        @Nullable final ProjectDTO repositoryProject = projectService.findById(project.getId());
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void clear() {
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
        projectService.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void getSize() {
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void addWithUserId() {
        projectService.add(USER_ID, project);
        Assert.assertEquals(1, projectService.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(EMPTY_USER_ID, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.add(USER_ID, NULL_PROJECT));
    }

    @Test
    public void existByIdWithUserId() {
        projectService.add(USER_ID, project);
        Assert.assertTrue(projectService.existsById(USER_ID, project.getId()));
        Assert.assertFalse(projectService.existsById(NULL_USER_ID, project.getId()));
        Assert.assertFalse(projectService.existsById(EMPTY_USER_ID, project.getId()));
        Assert.assertFalse(projectService.existsById(USER_ID, NULL_PROJECT_ID));
        Assert.assertFalse(projectService.existsById(USER_ID, EMPTY_PROJECT_ID));
    }

    @Test
    public void findAllWithUserId() {
        projectService.add(USER_ID, project);
        Assert.assertEquals(1, projectService.findAll(USER_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void findByIdWithUserId() {
        projectService.add(USER_ID, project);
        @Nullable final ProjectDTO repositoryProject = projectService.findById(USER_ID, project.getId());
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findById(NULL_USER_ID, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findById(EMPTY_USER_ID, project.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(USER_ID, NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(USER_ID, EMPTY_PROJECT_ID));
    }

    @Test
    public void removeWithUserId() {
        projectService.add(USER_ID, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(EMPTY_USER_ID, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.remove(USER_ID, NULL_PROJECT));
        projectService.remove(USER_ID, project);
        @Nullable final ProjectDTO repositoryProject = projectService.findById(USER_ID, project.getId());
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void removeByIdWithUserId() {
        projectService.add(USER_ID, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(NULL_USER_ID, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(EMPTY_USER_ID, project.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(USER_ID, NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(USER_ID, EMPTY_PROJECT_ID));
        projectService.removeById(USER_ID, project.getId());
        @Nullable final ProjectDTO repositoryProject = projectService.findById(USER_ID, project.getId());
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void clearWithUserId() {
        projectService.add(USER_ID, project);
        Assert.assertEquals(1, projectService.getSize(USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(EMPTY_USER_ID));
        projectService.clear(USER_ID);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        projectService.add(USER_ID, project);
        Assert.assertEquals(1, projectService.getSize(USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(EMPTY_USER_ID));
        projectService.clear(USER_ID);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void create() {
        projectService.add(USER_ID, project);
        Assert.assertEquals(1, projectService.getSize(USER_ID));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectService.create(NULL_USER_ID, PROJECT_NAME_PREFIX)
        );
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID, NULL_PROJECT_NAME));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.create(USER_ID, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION)
        );
    }

    @Test
    public void updateById() {
        projectService.add(USER_ID, project);
        Assert.assertEquals(1, projectService.getSize(USER_ID));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectService.updateById(
                        NULL_USER_ID, project.getId(),
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectService.updateById(
                        USER_ID, NULL_PROJECT_ID,
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateById(USER_ID, project.getId(), NULL_PROJECT_NAME, PROJECT_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateById(USER_ID, project.getId(), PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION)
        );
    }

    @Test
    public void changeStatusById() {
        projectService.add(USER_ID, project);
        Assert.assertEquals(1, projectService.getSize(USER_ID));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectService.changeProjectStatusById(NULL_USER_ID, project.getId(), Status.NOT_STARTED)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectService.changeProjectStatusById(USER_ID, NULL_PROJECT_ID, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> projectService.changeProjectStatusById(USER_ID, project.getId(), NULL_STATUS)
        );
    }

    @NotNull
    private ProjectDTO createOneProject() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(PROJECT_ID);
        project.setUserId(USER_ID);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private List<ProjectDTO> createManyProjects() {
        @NotNull final List<ProjectDTO> projects = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            ProjectDTO project = new ProjectDTO();
            project.setName(PROJECT_NAME_PREFIX + i);
            project.setDescription(PROJECT_DESCRIPTION_PREFIX + i);
            project.setUserId(USER_ID);
            projects.add(project);
        }
        return projects;
    }

    private void createUser() {
        @NotNull final UserDTO user = new UserDTO();
        user.setId(USER_ID);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        userService.add(user);
    }

}
