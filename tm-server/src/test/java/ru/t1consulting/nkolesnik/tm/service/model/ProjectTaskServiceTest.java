package ru.t1consulting.nkolesnik.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectTaskService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.model.ITaskService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IUserService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.UUID;

public class ProjectTaskServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String TASK_ID = UUID.randomUUID().toString();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private Task task;

    @NotNull
    private User user;

    @Nullable
    private Project project;

    @NotNull
    private Task createOneTask() {
        @NotNull final Task task = new Task();
        task.setId(TASK_ID);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setUser(user);
        return task;
    }

    @Before
    public void setup() {
        user = createUser();
        project = createOneProject();
        task = createOneTask();
    }

    @After
    public void cleanup() {
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void bindTaskToProject() {
        projectService.add(USER_ID, project);
        taskService.add(USER_ID, task);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(NULL_USER_ID, TASK_ID, PROJECT_ID)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, NULL_TASK_ID)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, NULL_PROJECT_ID, TASK_ID)
        );
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, task.getId());
        @Nullable final Task repositoryTask = taskService.findById(TASK_ID);
        Assert.assertNotNull(repositoryTask);
        Assert.assertNotNull(repositoryTask.getProject());
        Assert.assertEquals(PROJECT_ID, repositoryTask.getProject().getId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(NULL_USER_ID, TASK_ID, PROJECT_ID)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, NULL_TASK_ID)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, NULL_PROJECT_ID, TASK_ID)
        );
        taskService.add(USER_ID, task);
        projectService.add(USER_ID, project);
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        @Nullable Task repositoryTask = taskService.findById(TASK_ID);
        Assert.assertNotNull(repositoryTask);
        Assert.assertNotNull(repositoryTask.getProject());
        Assert.assertEquals(PROJECT_ID, repositoryTask.getProject().getId());
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        repositoryTask = taskService.findById(TASK_ID);
        Assert.assertNotNull(repositoryTask);
        Assert.assertNull(repositoryTask.getProject());
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(PROJECT_ID);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        project.setUser(user);
        return project;
    }

    private User createUser() {
        @NotNull final User user = new User();
        user.setId(USER_ID);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        userService.add(user);
        return user;
    }

}
