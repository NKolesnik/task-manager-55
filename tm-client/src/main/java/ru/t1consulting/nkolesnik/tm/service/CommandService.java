package ru.t1consulting.nkolesnik.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.ICommandRepository;
import ru.t1consulting.nkolesnik.tm.api.service.ICommandService;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.exception.system.ArgumentNotSupportedException;
import ru.t1consulting.nkolesnik.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.Collections;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        @Nullable Collection<AbstractCommand> collection = commandRepository.getTerminalCommands();
        if (collection == null || collection.isEmpty()) return Collections.emptyList();
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) throw new CommandNotSupportedException();
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        @Nullable AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException();
        return command;
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) throw new ArgumentNotSupportedException();
        @Nullable AbstractCommand command = commandRepository.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException();
        return command;
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArguments() {
        return commandRepository.getCommandsWithArguments();
    }

}

