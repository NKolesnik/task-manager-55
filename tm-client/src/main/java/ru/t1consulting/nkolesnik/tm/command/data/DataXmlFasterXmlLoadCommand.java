package ru.t1consulting.nkolesnik.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataXmlFasterXmlLoadRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

@Component
public final class DataXmlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-fasterxml-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data from XML file using FasterXML.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlFasterXmlLoadRequest(getToken()));
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
