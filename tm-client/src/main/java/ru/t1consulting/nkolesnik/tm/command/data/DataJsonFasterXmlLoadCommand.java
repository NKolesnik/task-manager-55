package ru.t1consulting.nkolesnik.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataJsonFasterXmlLoadRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

@Component
public final class DataJsonFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-fasterxml-json";

    @NotNull
    public static final String DESCRIPTION = "Load data from JSON file using FasterXML.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().loadDataJsonFasterXml(new DataJsonFasterXmlLoadRequest(getToken()));
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
