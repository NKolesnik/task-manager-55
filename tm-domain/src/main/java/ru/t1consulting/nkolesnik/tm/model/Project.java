package ru.t1consulting.nkolesnik.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.model.IWBS;
import ru.t1consulting.nkolesnik.tm.listener.EntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "projects")
@EntityListeners(EntityListener.class)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractWbsModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

}
